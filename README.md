# Happy 2017 !
This repository contains source of our happy 2017 spot:  
[Happy 2017 RGBa spot](https://www.youtube.com/watch?v=8jHO3_hHd1U)

![](./happy2017.png)

# Who are we ?
[RGBa](http://rgba.fr) is a collective of artists, working mainly with open source tools.

# Blender

#### Version
Blender 2.78a is used

#### Color Management
[Filmic View and Look Transformations for Blender](https://github.com/sobotka/filmic-blender) is used for renders. You can find a copy of files on post production folder. Follow instructions to install them.

#### Render Files
Render files are not included in this repository.  
Because we don't use a render farm, but multiple computers, you may have to adjust render output directories and/or render layers.

# Natron

#### Version
Natron 2.1.7 is used

#### Community plugins
We used some plugins not included in Natron be default. You can find them on [Natron community plugins page](https://github.com/NatronVFX/natron-plugins).  

#### Exr files
Render files from Blender are not included, and you will have to change input file directory to fit your configuration.

# Sound
Sounds are not included in this repository. Most of sounds are from [freesound](https://www.freesound.org/) and [Sound Fishing](http://www.sound-fishing.net/)

# Copyright

#### Sintel
Sintel is © Blender Foundation | www.sintel.org

#### Happy 2017
Our work is available under Creative Commons license © www.RGBa.fr (BY-SA)

# Any questions ?
Don't hesitate to contact us via :

*	[RGBa.fr](http://RGBa.fr)
*	[Twitter](https://twitter.com/RGBa_fr)
* [Facebook](https://www.facebook.com/RGBa-1474454206197114)
